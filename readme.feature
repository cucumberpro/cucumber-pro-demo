Feature: Cucumber Pro Demo

  This is an example of a Cucumber feature that you might find in any project.

  Go ahead and try hacking around. Add a new scenario, or even a few feature file. 
  Try saving your changes back to source control.

  Scenario: The one where something zen happens
    Given I have never used Cucumber Pro before
    When I play with this demo project
    Then I achieve enlightenment
